# QJimbo
QJimbo - is an movie application , which provides an opportunity to preview descriptions of films , trailers , imdb rating and others.
This is my fourth project in Android development industry. The purpose of this project was to built an app, to help users discover popular and highly rated movies on the web . It displays a scrolling view of popular ,highly rated movies, launches a details screen whenever a particular movie is selected, allows users to save favorites, play trailers, and read description. This app has sort films by genre ,interesting posters. I was trying to use OkHttp client to download movies from server. This app using SQlite Database to register and download all informations from web. 


<img width="40%" src="https://user-images.githubusercontent.com/39089390/52482947-4b142180-2bbb-11e9-9a5e-6deb4cbc6092.jpg" />      <img width="40%" src="https://user-images.githubusercontent.com/39089390/52483195-f7560800-2bbb-11e9-8cdb-5d0b18b99671.jpg" />
<img width="40%" src="https://user-images.githubusercontent.com/39089390/52484048-0ccc3180-2bbe-11e9-8c46-3e0cb61f2a9f.jpg" />     <img width="40%" src="https://user-images.githubusercontent.com/39089390/52484338-bd3a3580-2bbe-11e9-9ee2-f210adf4ff7d.jpg" />

### Fix in the near future

I'll try to fix and rebuild this app on TheMoviedb.org API , create web server to identify client.
