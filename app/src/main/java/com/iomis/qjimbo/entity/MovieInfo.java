package com.iomis.qjimbo.entity;

public class MovieInfo {
    public int id;
    public String movieName;
    public String imageLink;
    public String movieGenre;
    public String description;
    public String year;
    public String imdbRate;
    public String trailerYoutubeApiKey;
    public boolean recomended;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImdbRate() {
        return imdbRate;
    }

    public void setImdbRate(String imdbRate) {
        this.imdbRate = imdbRate;
    }

    public String getTrailerYoutubeApiKey() {
        return trailerYoutubeApiKey;
    }

    public void setTrailerYoutubeApiKey(String trailerYoutubeApiKey) {
        this.trailerYoutubeApiKey = trailerYoutubeApiKey;
    }

    public boolean isRecomended() {
        return recomended;
    }

    public void setRecomended(boolean recomended) {
        this.recomended = recomended;
    }
}
