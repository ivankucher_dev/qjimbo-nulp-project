package com.iomis.qjimbo.utils;

import android.os.Build;
import android.support.annotation.RequiresApi;
import com.iomis.qjimbo.domain.AppData;
import com.iomis.qjimbo.domain.AppDataManager;
import com.iomis.qjimbo.domain.MovieInfoDao;
import com.iomis.qjimbo.domain.PostersDao;
import com.iomis.qjimbo.entity.Genre;
import com.iomis.qjimbo.entity.InterestingPost;
import com.iomis.qjimbo.entity.MovieInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InfoForCards {

    public ArrayList<String> mNames = new ArrayList<>();
    public ArrayList<String> mGenre = new ArrayList<>();
    public ArrayList<String> mImageUrls = new ArrayList<>();
    public ArrayList<String> Description = new ArrayList<>();
    public ArrayList<String> year = new ArrayList<>();
    public ArrayList<String> imdb = new ArrayList<>();
    public ArrayList<String> TrailerUrls = new ArrayList<>();
    private MovieInfoDao movieInfoDao;
    private PostersDao postersDao;

    public InfoForCards() {
        movieInfoDao = new MovieInfoDao();
        postersDao = new PostersDao();
    }

    public ArrayList<String> getTrailerUrls() {
        return TrailerUrls;
    }

    public ArrayList<String> getYear() {
        return year;
    }

    public ArrayList<String> getImdb() {
        return imdb;
    }

    public ArrayList<String> getmNames() {
        return mNames;
    }

    public ArrayList<String> getmGenre() {
        return mGenre;
    }

    public ArrayList<String> getmImageUrls() {
        return mImageUrls;
    }

    public ArrayList<String> getDescription() {
        return Description;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void NewFilmsInitImageBitmaps() {
        List<MovieInfo> movieInfos = AppDataManager.getAppData().getNewMovieInfos();
        initListsByMovieObjects(movieInfos);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void RecomendedFilmsInitImageBitmaps() {
        List<MovieInfo> movieInfos = AppDataManager.getAppData().getRecommendedMovieInfos();
        initListsByMovieObjects(movieInfos);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initListsByMovieObjects(List<MovieInfo> movieInfos) {
        mNames.addAll(movieInfos.stream().map(MovieInfo::getMovieName).collect(Collectors.toList()));
        mGenre.addAll(movieInfos.stream().map(MovieInfo::getMovieGenre).collect(Collectors.toList()));
        mImageUrls.addAll(movieInfos.stream().map(MovieInfo::getImageLink).collect(Collectors.toList()));
        Description.addAll(movieInfos.stream().map(MovieInfo::getDescription).collect(Collectors.toList()));
        year.addAll(movieInfos.stream().map(MovieInfo::getYear).collect(Collectors.toList()));
        imdb.addAll(movieInfos.stream().map(MovieInfo::getImdbRate).collect(Collectors.toList()));
        movieInfos.stream().map(MovieInfo::getTrailerYoutubeApiKey).forEach(key -> TrailerUrls.add(key));
    }

    public void InitPosters() {
        mImageUrls.addAll(AppDataManager.getAppData().getPosters());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void InitInterestingPosts() {
        List<InterestingPost> interestingPosts = AppDataManager.getAppData().getInterestingPosts();
        mNames.addAll(interestingPosts.stream().map(InterestingPost::getName).collect(Collectors.toList()));
        mImageUrls.addAll(interestingPosts.stream().map(InterestingPost::getUrl).collect(Collectors.toList()));
    }


    public void InitGenres() {
        List<Genre> genres = AppDataManager.getAppData().getGenres();
        mNames.addAll(genres.stream().map(Genre::getGenre).collect(Collectors.toList()));
        mImageUrls.addAll(genres.stream().map(Genre::getUrl).collect(Collectors.toList()));
    }


}
