package com.iomis.qjimbo.utils;

import android.support.v7.widget.RecyclerView;

import java.util.TimerTask;

public class AutoScrollTask extends TimerTask {


    private int pos;
    private int arraysize;
    boolean end;
    RecyclerView recyclerView;

    public AutoScrollTask(RecyclerView rv, int position, int size) {
        pos = position;
        arraysize = size;
        recyclerView = rv;

    }

    @Override
    public void run() {
        if (pos == arraysize - 1) {
            end = true;
        } else if (pos == 0) {
            end = false;
        }
        if (!end) {
            pos++;
        } else {
            pos--;
        }
        recyclerView.smoothScrollToPosition(pos);
    }
}
