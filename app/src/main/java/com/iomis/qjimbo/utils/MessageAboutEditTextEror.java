package com.iomis.qjimbo.utils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

public class MessageAboutEditTextEror {

    public void ErorMessageInputInfo(AlertDialog.Builder EditTextFieldNullException, String Message, String TitleErorText) {

        EditTextFieldNullException.setTitle(TitleErorText) //title for Eror Message
                .setMessage(Message)
                .setPositiveButton("OK", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert = EditTextFieldNullException.create();
        alert.show();
        EditTextFieldNullException.setCancelable(true);

    }

    public void ErorMessageEmail(AlertDialog.Builder EditTextFieldNullException, String Message, String TitleErorText, final Intent intent, final AppCompatActivity activity) {


        EditTextFieldNullException.setTitle(TitleErorText) //title for Eror Message
                .setMessage(Message)
                .setPositiveButton("try again", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Sign in", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        activity.startActivity(intent);
                    }
                });


        AlertDialog alert = EditTextFieldNullException.create();
        alert.show();
        EditTextFieldNullException.setCancelable(true);

    }

}
