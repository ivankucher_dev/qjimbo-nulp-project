package com.iomis.qjimbo.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.UI.Trailer;
import com.iomis.qjimbo.utils.DBHelper;

public class InfoFilmActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "InoFilmActivity";
    private static final String TABLE_WATCHLIST = "WatchList";
    private static final String COLUMN_WATCHLISTID = "watchlistid";
    private static final String TABLE_FAVOURITES = "Favourites";
    private static final String COLUMN_FAVOURITESID = "favouritesid";

    private String imageUrl, imageName, imageGenre, imageDescr, imageYear, imageImdb, trailerurls;
    BottomNavigationView mBottomnav;
    ContentValues cv;
    DBHelper dbHelper;
    SQLiteDatabase db;
    Cursor c;
    ImageView imagetrailer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_film);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getIncomingIntent();
        initBottomNavigationClick();
        imagetrailer = (ImageView) findViewById(R.id.film_image);
        imagetrailer.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.film_image:

                Intent intent = new Intent(this, Trailer.class);
                intent.putExtra("trailer", trailerurls);
                Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(),
                        android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
                startActivity(intent, bundle);

                break;


        }
    }


    private void initBottomNavigationClick() {

        mBottomnav = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        mBottomnav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectFragment(item);
                return true;
            }
        });


    }


    private void selectFragment(MenuItem item) {
        Fragment frag = null;
        // init corresponding fragment
        switch (item.getItemId()) {
            case R.id.action_back:
                super.onBackPressed();
                break;
            case R.id.action_watch_list:
                addToDB(TABLE_WATCHLIST, COLUMN_WATCHLISTID);
                break;
            case R.id.action_favourites:
                addToDB(TABLE_FAVOURITES, COLUMN_FAVOURITESID);
                break;
        }

    }


    private void getIncomingIntent() {
        Log.d(TAG, "getIncomingIntent: checking for incoming intents.");

        if (getIntent().hasExtra("image_url") && getIntent().hasExtra("image_name") &&
                getIntent().hasExtra("image_genre") && getIntent().hasExtra("image_descr") &&
                getIntent().hasExtra("image_year") && getIntent().hasExtra("image_imdb")) {
            Log.d(TAG, "getIncomingIntent: found intent extras.");


            imageUrl = getIntent().getStringExtra("image_url");
            imageName = getIntent().getStringExtra("image_name");
            imageGenre = getIntent().getStringExtra("image_genre");
            imageDescr = getIntent().getStringExtra("image_descr");
            imageYear = getIntent().getStringExtra("image_year");
            imageImdb = getIntent().getStringExtra("image_imdb");
            trailerurls = getIntent().getStringExtra("trailer");


            setImage(imageUrl, imageName, imageGenre, imageYear, imageImdb, imageDescr);
        }
    }

    private void addToDB(String TABLE_NAME, String COLUMN_NAME) {

        cv = new ContentValues();
        dbHelper = new DBHelper(getApplicationContext());
        db = dbHelper.getWritableDatabase();
        //Check in database email is already in use
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE "
                + COLUMN_NAME + " =\'" + this.imageName + "\'";
        c = db.rawQuery(selectQuery, null);


        if (c.getCount() == 0) {
            cv.put(COLUMN_NAME, this.imageName);
            //Insert in Database
            long rowID = db.insert(TABLE_NAME, null, cv);
            Log.d("myLogs", "row inserted, ID = " + rowID);
            db.close();
            Toast.makeText(getApplicationContext(), imageName + " added successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), imageName + " is already in the list", Toast.LENGTH_SHORT).show();
        }

    }


    private void setImage(String imageUrl, String imageName, String imageGenre, String imageYear, String imageImdb, String imageDescr) {
        Log.d(TAG, "setImage: setting te image and name to widgets.");

        TextView name = findViewById(R.id.film_name);
        name.setText(imageName);
        TextView year = findViewById(R.id.film_year);
        year.setText("(" + imageYear + ")");
        TextView genre = findViewById(R.id.film_genre);
        genre.setText("Genre : " + imageGenre);
        TextView imdb = findViewById(R.id.film_imdb);
        imdb.setText("\n" + imageImdb);
        TextView description = findViewById(R.id.film_descr);
        description.setText("Description :\n " + imageDescr);


        ImageView image = findViewById(R.id.film_image);
        Glide.with(this)
                .load(imageUrl)
                .asBitmap()
                .into(image);
    }
}
