package com.iomis.qjimbo.activity;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.UI.SpacesItemsDecoration;
import com.iomis.qjimbo.adapters.RecyclerViewAdapter;
import com.iomis.qjimbo.utils.InfoForCards;

import java.util.ArrayList;
import java.util.List;


public class GenreActivity extends AppCompatActivity {

    private static final String TAG = FilmActivity.class.getSimpleName();
    private List<Movie> movies;
    private RecyclerView recyclerView;
    TextView genretext;
    String Genre;

    int resourceCardNewFilms;
    Bundle bundle;

    Intent intent;

    private ArrayList<String> mImageNames = new ArrayList<>();
    private ArrayList<String> mImages = new ArrayList<>();
    private ArrayList<String> mGenre = new ArrayList<>();
    private ArrayList<String> descr = new ArrayList<>();
    private ArrayList<String> year = new ArrayList<>();
    private ArrayList<String> imdb = new ArrayList<>();
    private ArrayList<String> trailer = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        resourceCardNewFilms = R.layout.card;
        Intent intent = getIntent();
        Genre = intent.getStringExtra("Genre");

        genretext = (TextView) findViewById(R.id.textGenre);
        genretext.setText(Genre);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.addItemDecoration(new SpacesItemsDecoration(25));
        recyclerView.setNestedScrollingEnabled(false);


        bundle = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(),
                android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
        InfoForCards InfoNewFilms = new InfoForCards();
        InfoNewFilms.NewFilmsInitImageBitmaps();
        InfoForCards InfoRecomendeFilms = new InfoForCards();
        InfoRecomendeFilms.RecomendedFilmsInitImageBitmaps();


        initImageBitmapsNewFilms(InfoNewFilms);
        initImageBitmapsNewFilms(InfoRecomendeFilms);


    }


    public void initImageBitmapsNewFilms(InfoForCards InfoNewFilms) {


        for (int j = 0; j < InfoNewFilms.mNames.size(); j++) {

            if (InfoNewFilms.mGenre.get(j).contains(this.Genre)) {

                mImageNames.add(InfoNewFilms.mNames.get(j));
                mGenre.add(InfoNewFilms.mGenre.get(j));
                mImages.add(InfoNewFilms.mImageUrls.get(j));
                descr.add(InfoNewFilms.Description.get(j));
                year.add(InfoNewFilms.year.get(j));
                imdb.add(InfoNewFilms.imdb.get(j));
                trailer.add(InfoNewFilms.TrailerUrls.get(j));


            }
        }


        initRecyclerView(resourceCardNewFilms, mImageNames, mImages, mGenre, descr, year, imdb, trailer);
    }


    private void initRecyclerView(int resource, ArrayList<String> mNames, ArrayList<String> mImageUrls, ArrayList<String> mGenre, ArrayList<String> description, ArrayList<String> year, ArrayList<String> imdb, ArrayList<String> TrailerUrls) {
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, mNames, mImageUrls, mGenre, description, year, imdb, TrailerUrls, resource, true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
    }


}
