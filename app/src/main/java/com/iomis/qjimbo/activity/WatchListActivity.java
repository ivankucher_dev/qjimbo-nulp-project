package com.iomis.qjimbo.activity;


import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.UI.SpacesItemsDecoration;
import com.iomis.qjimbo.adapters.RecyclerViewAdapter;
import com.iomis.qjimbo.utils.DBHelper;
import com.iomis.qjimbo.utils.InfoForCards;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class WatchListActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = FilmActivity.class.getSimpleName();
    private static final String TABLE_NAME_WL = "WatchList";
    private int GALLERY = 1;
    private List<Movie> movies;
    private RecyclerView recyclerView;
    byte[] avatarByteArr;
    Toolbar toolbar;
    TabLayout tabLayout;
    private GridLayoutManager gridLayout;
    private MoviesAdapter adapter;
    TextView username, nav_email;
    String nickname, emailfromdb;

    int rowID;
    ImageView avatar;
    int resourceCardNewFilms;
    int resourceCardRecom;
    Bundle bundle;

    private ArrayList<String> mImageNames = new ArrayList<>();
    private ArrayList<String> mImages = new ArrayList<>();
    private ArrayList<String> mGenre = new ArrayList<>();
    private ArrayList<String> descr = new ArrayList<>();
    private ArrayList<String> year = new ArrayList<>();
    private ArrayList<String> imdb = new ArrayList<>();
    private ArrayList<String> trailer = new ArrayList<>();
    private ArrayList<String> MovieName = new ArrayList<>();


    //User info
    ContentValues cv;
    SQLiteDatabase db;
    DBHelper dbHelper;
    DatabaseFinder dbfinder;
    Cursor cursor, avatarCursor;
    View headerView;
    BottomNavigationView mBottomNav;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_list);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        resourceCardNewFilms = R.layout.wl_card;


        cv = new ContentValues();
        dbfinder = new DatabaseFinder();
        initNavigationView();
        updateAvatar();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.addItemDecoration(new SpacesItemsDecoration(25));
        recyclerView.setNestedScrollingEnabled(false);
        movies = new ArrayList<>();
        // getMoviesFromDB(0);


        bundle = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(),
                android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
        InfoForCards InfoNewFilms = new InfoForCards();
        InfoNewFilms.NewFilmsInitImageBitmaps();
        InfoForCards InfoRecomendeFilms = new InfoForCards();
        InfoRecomendeFilms.RecomendedFilmsInitImageBitmaps();

        getWatchListFromDB();
        initBottomNavigationClick();
        initImageBitmapsNewFilms(InfoNewFilms);
        initImageBitmapsNewFilms(InfoRecomendeFilms);

    }


    public void initImageBitmapsNewFilms(InfoForCards InfoNewFilms) {

        for (int i = 0; i < MovieName.size(); i++) {

            for (int j = 0; j < InfoNewFilms.mNames.size(); j++) {

                if (this.MovieName.get(i).equals(InfoNewFilms.mNames.get(j))) {

                    mImageNames.add(InfoNewFilms.mNames.get(j));
                    mGenre.add(InfoNewFilms.mGenre.get(j));
                    mImages.add(InfoNewFilms.mImageUrls.get(j));
                    descr.add(InfoNewFilms.Description.get(j));
                    year.add(InfoNewFilms.year.get(j));
                    imdb.add(InfoNewFilms.imdb.get(j));
                    trailer.add(InfoNewFilms.TrailerUrls.get(j));
                }

            }
        }


        initRecyclerView(resourceCardNewFilms, mImageNames, mImages, mGenre, descr, year, imdb, trailer);
    }

    private void initBottomNavigationClick() {

        mBottomNav = findViewById(R.id.bottom_navigation);
        mBottomNav.setSelectedItemId(R.id.action_watch_list);

        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectFragment(item);
                return true;
            }
        });


    }


    private void selectFragment(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_catalog:
                Intent intent = new Intent(WatchListActivity.this, FilmActivity.class);
                startActivity(intent, bundle);
                finish();
                break;
            case R.id.action_watch_list:

                break;
            case R.id.action_favourites:
                Intent intent2 = new Intent(WatchListActivity.this, FavouritesActivity.class);
                startActivity(intent2, bundle);
                finish();
                break;
        }


    }


    public void getWatchListFromDB() {
        dbHelper = new DBHelper(this);
        db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_WL, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {

                this.MovieName.add(cursor.getString(1));


            } while (cursor.moveToNext());


        }

        cursor.close();
        db.close();


    }

    private void initNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);


    }


    private void initRecyclerView(int resource, ArrayList<String> mNames, ArrayList<String> mImageUrls, ArrayList<String> mGenre, ArrayList<String> description, ArrayList<String> year, ArrayList<String> imdb, ArrayList<String> trailerUrls) {
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, mNames, mImageUrls, mGenre, description, year, imdb, trailerUrls, resource, false);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
    }


    public void updateAvatar() {
        if (avatarByteArr != null) {


            Bitmap mBitmap = BitmapFactory.decodeByteArray(avatarByteArr, 0, avatarByteArr.length);
            avatar.setImageBitmap(mBitmap);
        }
    }


    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    avatar.setImageBitmap(bitmap);

                    byte[] avatarByteArr = getBitmapAsByteArray(bitmap);
                    cv.put("avatar", avatarByteArr);
                    dbHelper = new DBHelper(this);
                    db = dbHelper.getWritableDatabase();
                    db.update("UserRegisterInfo", cv, " email = '" + emailfromdb + "'", null);
                    db.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, outputStream);
        return outputStream.toByteArray();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.avatar:
                choosePhotoFromGallary();

                break;
        }

    }


   /*    private void getMoviesFromDB(int id) {

        AsyncTask<Integer, Void, Void> asyncTask = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... movieIds) {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://192.168.43.161/movies.php?id=" + movieIds[0])
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONArray array = new JSONArray(response.body().string());

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);

                        Movie movie = new Movie(object.getInt("id"), object.getString("movie_name"),
                                object.getString("movie_image"), object.getString("movie_genre"));

                        FilmActivity.this.movies.add(movie);

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                adapter.notifyDataSetChanged();
            }
        };

        asyncTask.execute(id);
    }*/


  /*  public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Toast.makeText(FilmActivity.this, "File Saved::--->" + f.getAbsolutePath(), Toast.LENGTH_SHORT).show();

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }*/





    /*private String getRealPathFromURI(Uri contentUri) {
        String[] store = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, store, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        if (cursor != null ) {
            cursor.close();
        }
        return result;
    }*/


}
