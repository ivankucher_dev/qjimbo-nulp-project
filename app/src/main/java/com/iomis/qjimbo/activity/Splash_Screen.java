package com.iomis.qjimbo.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.domain.AppDataManager;

public class Splash_Screen extends AppCompatActivity {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ImageView animationTarget = this.findViewById(R.id.logo);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        animationTarget.startAnimation(animation);

        intent = new Intent(this, MainActivity.class);
        //New thread (Waiting add information to our DB)
        Handler handler = new Handler();
        Runnable runnable = AppDataManager::downloadData;
        Thread downloadThread = new Thread(runnable);
        downloadThread.start();
        handler.postDelayed(() -> {
            Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(),
                    android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
            startActivity(intent, bundle);
            finish();
            try {
                downloadThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, 2500);
    }
}
