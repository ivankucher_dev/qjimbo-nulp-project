package com.iomis.qjimbo.activity;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.iomis.qjimbo.R;
import com.iomis.qjimbo.utils.DBHelper;
import com.iomis.qjimbo.utils.InfoForCards;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CartoonActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = FilmActivity.class.getSimpleName();
    private BottomNavigationView mBottomNav;

    private int GALLERY = 1;
    private List<Movie> movies;
    private RecyclerView recyclerView;
    byte[] avatarByteArr;
    TabLayout tabLayout;
    TextView username, nav_email;
    String nickname, emailfromdb;
    ImageView avatar;
    int resourceCardNewFilms;
    int resourceCardRecom;
    Bundle bundle;

    //User info
    ContentValues cv;
    SQLiteDatabase db;
    DBHelper dbHelper;
    DatabaseFinder dbfinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartoon);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        //set your username on navigation drawer
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        username = headerView.findViewById(R.id.nav_username);
        avatar = headerView.findViewById(R.id.avatar);
        nav_email = headerView.findViewById(R.id.nav_email);


        avatar.setOnClickListener(this);
        //get username from intent from db
        Intent intent = getIntent();
        nickname = intent.getStringExtra("Username");
        emailfromdb = intent.getStringExtra("Email");
        avatarByteArr = intent.getByteArrayExtra("Avatar");
        username.setText(nickname);
        nav_email.setText(emailfromdb);
        resourceCardNewFilms = R.layout.card;
        resourceCardRecom = R.layout.long_card;


        cv = new ContentValues();
        dbfinder = new DatabaseFinder();
        updateAvatar();
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setNestedScrollingEnabled(false);
        movies = new ArrayList<>();

        bundle = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(),
                android.R.anim.fade_in, android.R.anim.fade_out).toBundle();

        initBottomNavigationClick();
        initImageBitmapsNewFilms();
        initInstancesDrawer();

    }


    private void initInstancesDrawer() {

        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Films"));
        tabLayout.addTab(tabLayout.newTab().setText("Serials"));
        tabLayout.addTab(tabLayout.newTab().setText("Cartoon"));
        tabLayout.getTabAt(2).select();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        Intent intentFilms = new Intent(CartoonActivity.this, FilmActivity.class);
                        startActivity(intentFilms);
                        finish();
                        break;
                    case 1:
                        Intent intentSerials = new Intent(CartoonActivity.this, SerialsActivity.class);
                        startActivity(intentSerials);
                        finish();
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    public void initImageBitmapsNewFilms() {
        InfoForCards InfoNewFilms = new InfoForCards();
        InfoNewFilms.NewFilmsInitImageBitmaps();

        initRecyclerView(resourceCardNewFilms, InfoNewFilms.getmNames(), InfoNewFilms.getmImageUrls(), InfoNewFilms.getmGenre(), InfoNewFilms.getDescription(), InfoNewFilms.getYear(), InfoNewFilms.getImdb());
    }


    private void initBottomNavigationClick() {

        mBottomNav = findViewById(R.id.bottom_navigation);
        mBottomNav.setSelectedItemId(R.id.action_catalog);
        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectFragment(item);
                return true;
            }
        });


    }


    private void selectFragment(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_catalog:

                break;
            case R.id.action_watch_list:
                Intent intent = new Intent(CartoonActivity.this, WatchListActivity.class);
                startActivity(intent, bundle);
                finish();
                break;
            case R.id.action_favourites:
                Intent intent2 = new Intent(CartoonActivity.this, FavouritesActivity.class);
                startActivity(intent2, bundle);
                finish();
                break;
        }


    }

    private void initRecyclerView(int resource, ArrayList<String> mNames, ArrayList<String> mImageUrls, ArrayList<String> mGenre, ArrayList<String> description, ArrayList<String> year, ArrayList<String> imdb) {
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recyclerview);

    }

    public void updateAvatar() {
        if (avatarByteArr != null) {


            Bitmap mBitmap = BitmapFactory.decodeByteArray(avatarByteArr, 0, avatarByteArr.length);
            avatar.setImageBitmap(mBitmap);
        }
    }


    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    avatar.setImageBitmap(bitmap);

                    byte[] avatarByteArr = getBitmapAsByteArray(bitmap);
                    cv.put("avatar", avatarByteArr);
                    dbHelper = new DBHelper(this);
                    db = dbHelper.getWritableDatabase();
                    db.update("UserRegisterInfo", cv, " email = '" + emailfromdb + "'", null);
                    db.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(CartoonActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, outputStream);
        return outputStream.toByteArray();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.avatar:
                choosePhotoFromGallary();

                break;
        }

    }


   /*    private void getMoviesFromDB(int id) {

        AsyncTask<Integer, Void, Void> asyncTask = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... movieIds) {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://192.168.43.161/movies.php?id=" + movieIds[0])
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONArray array = new JSONArray(response.body().string());

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);

                        Movie movie = new Movie(object.getInt("id"), object.getString("movie_name"),
                                object.getString("movie_image"), object.getString("movie_genre"));

                        FilmActivity.this.movies.add(movie);

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                adapter.notifyDataSetChanged();
            }
        };

        asyncTask.execute(id);
    }*/


  /*  public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Toast.makeText(FilmActivity.this, "File Saved::--->" + f.getAbsolutePath(), Toast.LENGTH_SHORT).show();

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }*/


    /*private String getRealPathFromURI(Uri contentUri) {
        String[] store = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, store, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        if (cursor != null ) {
            cursor.close();
        }
        return result;
    }*/


}
