package com.iomis.qjimbo.domain;

public class AppDataManager {
    private static AppData appData;

    public static void downloadData() {
        appData = new AppData();
        appData.downloadDataFromDb();
    }
    public static AppData getAppData() {
        return appData == null ? new AppData() : appData;
    }
}
