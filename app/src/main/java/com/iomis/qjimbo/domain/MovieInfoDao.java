package com.iomis.qjimbo.domain;

import com.iomis.qjimbo.entity.MovieInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MovieInfoDao {

    public static final String MOVIE_NAME_COLUMN = "movie_name";
    public static final String IMAGE_LINK_COLUMN = "image_link";
    public static final String GENRE_COLUMN = "movie_genre";
    public static final String DESCR_COLUMN = "description";
    public static final String YEAR_COLUMN = "year";
    public static final String IMDB_COLUMN = "imdb_rate";
    public static final String YOUTUBE_API_COLUMN = "trailer_youtube_api_key";
    public static final String RECOMMENDED_COLUMN = "recomended";

    private static final String SELECT_QUERY_RECOMMENDED_STATUS = "SELECT * FROM movie WHERE recomended = ?";
    private static final String INSERT_INTO_QUERY = "INSERT INTO movie(id,movie_name,image_link,movie_genre,year,trailer_youtube_api_key) " + "VALUES(?,?,?,?,?,?,?)";

    public List<MovieInfo> findAllRecommendedStatusMovies(boolean recommended) {
        List<MovieInfo> movieInfos = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getDBConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_QUERY_RECOMMENDED_STATUS);
            preparedStatement.setBoolean(1, recommended);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                MovieInfo movieInfo = new MovieInfo();
                movieInfo.setDescription(resultSet.getString(DESCR_COLUMN));
                movieInfo.setImageLink(resultSet.getString(IMAGE_LINK_COLUMN));
                movieInfo.setImdbRate(resultSet.getString(IMDB_COLUMN));
                movieInfo.setMovieGenre(resultSet.getString(GENRE_COLUMN));
                movieInfo.setMovieName(resultSet.getString(MOVIE_NAME_COLUMN));
                movieInfo.setRecomended(resultSet.getBoolean(RECOMMENDED_COLUMN));
                movieInfo.setTrailerYoutubeApiKey(resultSet.getString(YOUTUBE_API_COLUMN));
                movieInfo.setYear(resultSet.getString(YEAR_COLUMN));
                movieInfos.add(movieInfo);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return movieInfos;
    }

    public boolean saveMovie(MovieInfo movieInfo) throws SQLException {
        Connection connection = DatabaseConnector.getDBConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_QUERY);
        preparedStatement.setInt(1, movieInfo.getId());
        preparedStatement.setString(2, movieInfo.getMovieName());
        preparedStatement.setString(3, movieInfo.getImageLink());
        preparedStatement.setString(4, movieInfo.getMovieGenre());
        preparedStatement.setString(5, movieInfo.getYear());
        preparedStatement.setString(6, movieInfo.getTrailerYoutubeApiKey());
        preparedStatement.executeQuery();
        return true;
    }


}
