package com.iomis.qjimbo.domain;

import android.os.StrictMode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {

    public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB = "innodb";
    public static final String URL = "jdbc:mysql://database-1.clhyqf8dzyxj.eu-central-1.rds.amazonaws.com:3306/" + DB   + "?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&serverTimezone=GMT";
    public static final String USER = "admin";
    public static final String PASSWORD = "H2so112756stas";

    public static Connection getDBConnection() {
        Connection dbConnection = null;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            Class.forName(DRIVER);
            dbConnection = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println(dbConnection);
            return dbConnection;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return dbConnection;
    }
}
