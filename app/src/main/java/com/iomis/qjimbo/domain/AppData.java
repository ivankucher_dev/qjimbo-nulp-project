package com.iomis.qjimbo.domain;

import com.iomis.qjimbo.entity.Genre;
import com.iomis.qjimbo.entity.InterestingPost;
import com.iomis.qjimbo.entity.MovieInfo;

import java.util.List;

public class AppData {

    private List<MovieInfo> newMovieInfos;
    private List<MovieInfo> recommendedMovieInfos;
    private List<String> posters;
    private List<InterestingPost> interestingPosts;
    private List<Genre> genres;
    private MovieInfoDao movieInfoDao;
    private PostersDao postersDao;

    public AppData() {
        movieInfoDao = new MovieInfoDao();
        postersDao = new PostersDao();
    }

    public void downloadDataFromDb() {
        newMovieInfos = movieInfoDao.findAllRecommendedStatusMovies(false);
        recommendedMovieInfos = movieInfoDao.findAllRecommendedStatusMovies(true);
        posters = postersDao.findAllPosters();
        interestingPosts = postersDao.findAllInterestingPosts();
        genres = postersDao.findAllGenres();
    }

    public List<MovieInfo> getNewMovieInfos() {
        return newMovieInfos;
    }

    public void setNewMovieInfos(List<MovieInfo> newMovieInfos) {
        this.newMovieInfos = newMovieInfos;
    }

    public List<MovieInfo> getRecommendedMovieInfos() {
        return recommendedMovieInfos;
    }

    public void setRecommendedMovieInfos(List<MovieInfo> recommendedMovieInfos) {
        this.recommendedMovieInfos = recommendedMovieInfos;
    }

    public List<String> getPosters() {
        return posters;
    }

    public void setPosters(List<String> posters) {
        this.posters = posters;
    }

    public List<InterestingPost> getInterestingPosts() {
        return interestingPosts;
    }

    public void setInterestingPosts(List<InterestingPost> interestingPosts) {
        this.interestingPosts = interestingPosts;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }
}
