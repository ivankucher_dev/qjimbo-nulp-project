package com.iomis.qjimbo.domain;

import com.iomis.qjimbo.entity.Genre;
import com.iomis.qjimbo.entity.InterestingPost;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostersDao {

    public static final String POSTER_LINK_COLUMN = "poster_url";
    public static final String NAME_COLUMN = "name";
    public static final String GENRE_COLUMN = "genre";
    public static final String URL_COLUMN = "url";
    private static final String DB_POSTERS = "posters";
    private static final String DB_GENRE = "genre";
    private static final String DB_INTERESTING_POST = "interesting_post";
    private static final String SELECT_QUERY = "SELECT * FROM ";
    private static final String SELECT_GENRE = SELECT_QUERY + DB_GENRE;
    private static final String SELECT_POSTERS = SELECT_QUERY + DB_POSTERS;
    private static final String SELECT_INTEREST_POST = SELECT_QUERY + DB_INTERESTING_POST;

    public List<String> findAllPosters() {
        List<String> posterLinks = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getDBConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_POSTERS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                posterLinks.add(resultSet.getString(POSTER_LINK_COLUMN));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return posterLinks;
    }

    public List<InterestingPost> findAllInterestingPosts() {
        List<InterestingPost> interestingPosts = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getDBConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_INTEREST_POST);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                InterestingPost interestingPost = new InterestingPost();
                interestingPost.setName(resultSet.getString(NAME_COLUMN));
                interestingPost.setUrl(resultSet.getString(URL_COLUMN));
                interestingPosts.add(interestingPost);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return interestingPosts;
    }

    public List<Genre> findAllGenres() {
        List<Genre> genres = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getDBConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_GENRE);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Genre genre = new Genre();
                genre.setGenre(resultSet.getString(GENRE_COLUMN));
                genre.setUrl(resultSet.getString(URL_COLUMN));
                genres.add(genre);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return genres;
    }

}
