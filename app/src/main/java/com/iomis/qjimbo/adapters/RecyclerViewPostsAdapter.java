package com.iomis.qjimbo.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.iomis.qjimbo.R;

import java.util.ArrayList;


public class RecyclerViewPostsAdapter extends RecyclerView.Adapter<RecyclerViewPostsAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";


    private ArrayList<String> mImages = new ArrayList<>();
    private ArrayList<String> mNames = new ArrayList<>();


    private Context mContext;


    private int resource;


    public RecyclerViewPostsAdapter(Context context, ArrayList<String> images, ArrayList<String> names, int res) {

        mImages = images;
        mNames = names;
        mContext = context;
        resource = res;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .load(mImages.get(position % mImages.size()))
                .asBitmap()
                .into(holder.image);
        holder.textPost.setText(mNames.get(position % mImages.size()));


    }


    @Override
    public int getItemCount() {
        return mImages == null ? 0 : mImages.size() * 2;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView image;
        TextView textPost;
        LinearLayout parentLayout;
        Activity activity;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.postImage);
            textPost = itemView.findViewById(R.id.textPost);
            parentLayout = itemView.findViewById(R.id.linearlayout);
            activity = (Activity) mContext;
            image.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            getAdapterPosition();
            switch (view.getId()) {
                case R.id.postImage:
                    Toast.makeText(mContext, "No longer avaliable", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;


            }
        }
    }


}
